﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MfiDesktopApp.Context;
using MfiDesktopApp.Dto;
using MfiDesktopApp.Enums;
using MfiDesktopApp.Models;

namespace MfiDesktopApp.Services
{
    public class SignInService
    {
        public static LoginDto SignIn(Account newAccount, Client newClient)
        {
            using (var db = new MfiContext())
            {
                // TODO: Сделать транзакцию (мб и не надо, если есть save changes)
                // TODO: Придумать что делать с хешированием пароля при регистрации
                db.Account.Add(newAccount);

                // WARNING!
                if (!db.Client.Any(c => c.PassportSerialNumber == newClient.PassportSerialNumber))
                {
                    newClient.Account = newAccount;
                    db.Client.Add(newClient);
                }
                else
                {
                    newClient = db.Client.First(c => c.PassportSerialNumber == newClient.PassportSerialNumber);
                    newClient.Account = newAccount;
                }

                db.SaveChanges();

                var login = newAccount.Login;
                var password = newAccount.Password;

                var loginData = LoginService.TryLogin(login, password);

                return loginData;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MfiDesktopApp.Context;
using MfiDesktopApp.Dto;
using MfiDesktopApp.Enums;
using MfiDesktopApp.Models;

namespace MfiDesktopApp.Services
{
    public class LoginService
    {
        public static LoginDto TryLogin(string login, string password)
        {
            if (!DoesAccountExists(login))
                throw new Exception("Неверный логин!");

            using (var db = new MfiContext())
            {
                // TODO: Хэшировать пароль
                var hashPassword = password;

                if (!db.Account.Any(a => a.Login == login && a.Password == hashPassword))
                    throw new Exception("Неверный пароль!");

                var account = db.Account
                    .First(a => a.Login == login && a.Password == hashPassword);

                // Фамилия и имя привязанные к учётке WARNING!
                var name = string.Empty;
                var surname = string.Empty;
                if (account.RoleId == (int)RoleEnum.Client)
                {
                    var person = db.Client
                        .First(c => c.AccountId == account.Id);
                    name = person.Name;
                    surname = person.Surname;
                }
                else
                {
                    var person = db.Employee
                       .First(e => e.AccountId == account.Id);
                    name = person.Name;
                    surname = person.Surname;
                }

                var result = new LoginDto()
                {
                    Login = account.Login,
                    Role = (RoleEnum)account.RoleId,
                    Name = name,
                    Surname = surname
                };

                return result;
            }
        }

        public static bool DoesAccountExists(string login)
        {
            using (var db = new MfiContext())
            {
                if (db.Account.Any(a => a.Login == login))
                    return true;

                return false;
            }
        }

        public static bool DoesClientExists(string passport)
        {
            using (var db = new MfiContext())
            {
                if (db.Client.Any(c => c.PassportSerialNumber == passport))
                    return true;

                return false;
            }
        }
    }
}

using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

using MfiDesktopApp.Models;

namespace MfiDesktopApp.Context
{
    public partial class MfiContext : DbContext
    {
        public MfiContext()
            : base("name=MfiContext")
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Collector> Collector { get; set; }
        public virtual DbSet<CollectorsAndLoans> CollectorsAndLoans { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Loan> Loan { get; set; }
        public virtual DbSet<LoanStatus> LoanStatus { get; set; }
        public virtual DbSet<OperationHistory> OperationHistory { get; set; }
        public virtual DbSet<OperationType> OperationType { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
        public virtual DbSet<PaymentStatus> PaymentStatus { get; set; }
        public virtual DbSet<Role> Role { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Employee)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.OperationHistory)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Sex)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.PassportSerialNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Loan)
                .WithRequired(e => e.Client)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Payment)
                .WithRequired(e => e.Client)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Collector>()
                .HasMany(e => e.CollectorsAndLoans)
                .WithRequired(e => e.Collector)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Sex)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Loan>()
                .Property(e => e.percent)
                .HasPrecision(5, 2);

            modelBuilder.Entity<Loan>()
                .HasMany(e => e.CollectorsAndLoans)
                .WithRequired(e => e.Loan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Loan>()
                .HasMany(e => e.Payment)
                .WithRequired(e => e.Loan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LoanStatus>()
                .HasMany(e => e.Loan)
                .WithRequired(e => e.LoanStatus)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OperationType>()
                .HasMany(e => e.OperationHistory)
                .WithRequired(e => e.OperationType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PaymentStatus>()
                .HasMany(e => e.Payment)
                .WithRequired(e => e.PaymentStatus)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Account)
                .WithRequired(e => e.Role)
                .WillCascadeOnDelete(false);
        }
    }
}

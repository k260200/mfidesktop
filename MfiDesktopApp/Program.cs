﻿using MfiDesktopApp.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MfiDesktopApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.DoEvents();

                var screenSplash = new ScreenSplash();
                screenSplash.ShowDialog();

                DoAuthorization();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // TODO: Придумать, как передавать контекст
        static void DoAuthorization()
        {
            var authentificationForm = new AuthForm();
            authentificationForm.ShowDialog();
            var loginData = authentificationForm.LoginData;
            if (loginData == null)
                return;

            switch (loginData.Role)
            {
                case Enums.RoleEnum.Client:
                    Application.Run(new ClientForm(loginData));
                    break;
                case Enums.RoleEnum.Cashier:
                    Application.Run(new CashierForm(loginData));
                    break;
                case Enums.RoleEnum.LoanManager:
                    Application.Run(new ManagerForm(loginData));
                    break;
                case Enums.RoleEnum.Director:
                    Application.Run(new DirectorForm(loginData));
                    break;
                default:
                    throw new Exception("Критическая ошибка! Неидентифицированная роль при авторизации!");
            }
        }
    }
}

namespace MfiDesktopApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CollectorsAndLoans
    {
        public int Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime dateOfTransfer { get; set; }

        public int CollectorId { get; set; }

        public int LoanId { get; set; }

        public virtual Collector Collector { get; set; }

        public virtual Loan Loan { get; set; }
    }
}

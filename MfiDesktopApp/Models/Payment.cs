namespace MfiDesktopApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Payment")]
    public partial class Payment
    {
        public int Id { get; set; }

        public decimal sum { get; set; }

        [Column(TypeName = "date")]
        public DateTime date { get; set; }

        public int LoanId { get; set; }

        public int StatusId { get; set; }

        public int ClientId { get; set; }

        public virtual Client Client { get; set; }

        public virtual Loan Loan { get; set; }

        public virtual PaymentStatus PaymentStatus { get; set; }
    }
}

namespace MfiDesktopApp.Models
{
    using MfiDesktopApp.Enums;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Client")]
    public partial class Client
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Client()
        {
            Loan = new HashSet<Loan>();
            Payment = new HashSet<Payment>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string Surname { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [StringLength(1)]
        public string Sex { get; set; }

        [Required]
        [StringLength(20)]
        public string PassportSerialNumber { get; set; }

        [Column(TypeName = "date")]
        public DateTime PassportDateOfIssue { get; set; }

        public decimal CurrentIncome { get; set; }

        public int? AccountId { get; set; }

        public virtual Account Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Loan> Loan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Payment> Payment { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public bool HasAccount
        {
            get
            {
                if (AccountId.HasValue)
                    return true;
                return false;
            }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int ActiveLoansCount
        {
            get
            {
                var result = 0;

                foreach (var loan in Loan)
                    if (loan.StatusId == (int)LoanStatusEnum.Active)
                        result++;

                return result;
            }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal CurrentDebt
        {
            get
            {
                var result = 0.0m;

                foreach (var loan in Loan)
                    if (loan.StatusId == (int)LoanStatusEnum.Active)
                        result += loan.CurrentSum;

                return result;
            }
        }
    }
}

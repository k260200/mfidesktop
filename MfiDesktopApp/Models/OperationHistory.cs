namespace MfiDesktopApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OperationHistory")]
    public partial class OperationHistory
    {
        public int Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime date { get; set; }

        public int OperationTypeId { get; set; }

        public int AccountId { get; set; }

        public int ObjectId { get; set; }

        public virtual Account Account { get; set; }

        public virtual OperationType OperationType { get; set; }
    }
}

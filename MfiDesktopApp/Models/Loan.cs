namespace MfiDesktopApp.Models
{
    using MfiDesktopApp.Enums;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Loan")]
    public partial class Loan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Loan()
        {
            CollectorsAndLoans = new HashSet<CollectorsAndLoans>();
            Payment = new HashSet<Payment>();
        }

        public int Id { get; set; }

        public decimal sum { get; set; }

        public decimal percent { get; set; }

        [Column(TypeName = "date")]
        public DateTime dateOfIssue { get; set; }

        public int? timeLimitationInMonths { get; set; }

        public int StatusId { get; set; }

        public int ClientId { get; set; }

        public virtual Client Client { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CollectorsAndLoans> CollectorsAndLoans { get; set; }

        public virtual LoanStatus LoanStatus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Payment> Payment { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? dateOfRedemotion
        {
            get
            {
                if (!timeLimitationInMonths.HasValue)
                    return null;

                return dateOfIssue.AddMonths(timeLimitationInMonths.Value);

            }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string LoanStatusName { get => LoanStatus.Name; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal CurrentSum { get => sum; } // TODO: ��������� ������� ��� �����

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal RedemptionSum { 
            get 
            {
                decimal redemption = 0.0m;

                foreach (var payment in Payment)
                    if (payment.StatusId == (int)PaymentStatusEnum.Active)
                        redemption += payment.sum;

                return redemption;
            } 
        }
    }
}

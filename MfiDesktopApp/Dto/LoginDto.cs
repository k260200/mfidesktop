﻿using MfiDesktopApp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MfiDesktopApp.Dto
{
    public class LoginDto
    {
        public string Login { get; set; }
        public RoleEnum Role { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}

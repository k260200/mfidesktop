﻿using MfiDesktopApp.Context;
using MfiDesktopApp.Dto;
using MfiDesktopApp.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MfiDesktopApp.Forms
{
    public partial class ClientForm : Form
    {
        // TODO: Заполнить пустое простанство
        // TODO: Понатыкать везде OrderBy
        // TODO: Кнопка для обновления данных
        // TODO: Возможность выхода
        public LoginDto LoginInfo { get; set; }
        public int ClientId { get; set; }

        public ClientForm(LoginDto loginInfo)
        {
            InitializeComponent();

            LoginInfo = loginInfo;
            ShowLoginData();

            ClientId = GetClientId();
            RefreshLoans();
        }

        private void btnTakeLoan_Click(object sender, EventArgs e)
        {
            var clientId = GetClientId();
            new TakeLoanForm(clientId).ShowDialog();
            RefreshLoans();
        }

        private void btnDoPayment_Click(object sender, EventArgs e)
        {
            if (!(dgvLoans.SelectedRows.Count > 0))
            {
                MessageBox.Show("Выберите в таблице кредит для оплаты!");
                return;
            }

            var selectedLoanRow = dgvLoans.SelectedRows[0];
            var loanId = (int)selectedLoanRow.Cells[0].Value;
            var clientId = GetClientId();

            new AddPaymentForm(clientId, loanId).ShowDialog();
            RefreshLoans();
        }

        private void dgvLoans_SelectionChanged(object sender, EventArgs e)
        {
            RefreshLoanInfo();
            RefreshPaymentInfo();
        }

        int GetClientId()
        {
            using (var db = new MfiContext())
            {
                return db.Client.First(c => c.Account.Login == LoginInfo.Login).Id;
            }
        }

        void RefreshLoans()
        {
            using (var db = new MfiContext())
            {
                var loans = db.Loan
                    .AsNoTracking()
                    .Where(l => l.ClientId == ClientId)
                    .Select(l => new // TODO: Добавить представление мб
                    {
                        Id = l.Id,
                        sum = l.sum,
                        percent = l.percent,
                        dateOfIssue = l.dateOfIssue,
                        timeLimitationInMonths = l.timeLimitationInMonths
                    })
                    .ToArray();
                dgvLoans.DataSource = loans;
            }
        }

        void RefreshLoanInfo()
        {
            if (dgvLoans.SelectedRows.Count > 0)
            {
                var selectedLoanRow = dgvLoans.SelectedRows[0];
                var loanId = (int)selectedLoanRow.Cells[0].Value;
                var clientId = GetClientId();

                using (var db = new MfiContext())
                {
                    var selectedLoan = db.Loan
                    .First(l => l.Id == loanId);
                    var loanInfo = new LoanInfo
                    {
                        Id = selectedLoan.Id,
                        Sum = selectedLoan.sum,
                        Percent = selectedLoan.percent,
                        CurrentSum = selectedLoan.CurrentSum,
                        Repaid = selectedLoan.RedemptionSum,
                        Taken = selectedLoan.dateOfIssue,
                        MustBeRepaid = selectedLoan.dateOfRedemotion,
                        Status = selectedLoan.LoanStatusName
                    };
                    loanInfo.Passport = db.Client.First(c => c.Id == clientId).PassportSerialNumber;
                    pgLoanInfo.SelectedObject = loanInfo;
                }
            }
        }

        void RefreshPaymentInfo()
        {
            if (dgvLoans.SelectedRows.Count > 0)
            {
                var selectedLoanRow = dgvLoans.SelectedRows[0];
                var loanId = (int)selectedLoanRow.Cells[0].Value;

                using (var db = new MfiContext())
                {
                    var selectedLoanPaymentRows = db.Loan
                    .First(l => l.Id == loanId)
                    .Payment
                    .Select(p => new // TODO: Сделать по нормальному
                    {
                        sum = p.sum,
                        date = p.date, // TODO: Придумать как убрать время
                        status = p.PaymentStatus.Name
                    });

                    // TODO: И это тоже
                    dgvLoanPayments.Rows.Clear();
                    foreach (var loanRow in selectedLoanPaymentRows)
                        dgvLoanPayments.Rows.Add(loanRow.sum, loanRow.date, loanRow.status);
                }
            }
        }

        void ShowLoginData()
        {
            lbName.Text = LoginInfo.Name;
            lbSurname.Text = LoginInfo.Surname;
            lbLogin.Text = LoginInfo.Login;
        }
    }
}

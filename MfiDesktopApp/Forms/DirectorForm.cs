﻿using MfiDesktopApp.Context;
using MfiDesktopApp.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MfiDesktopApp.Forms
{
    public partial class DirectorForm : Form
    {
        public LoginDto LoginInfo { get; set; }

        public DirectorForm(LoginDto loginInfo)
        {
            InitializeComponent();

            LoginInfo = loginInfo;
            ShowLoginData();

            RefreshClients();
            cbMonth.SelectedIndex = 0; // Заполнено в свойствах
            RefreshChart();
        }

        private void btnHire_Click(object sender, EventArgs e)
        {
            try
            {
                new AddEditEmployeeForm().ShowDialog();
                RefreshClients();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(dgvEmployees.SelectedRows.Count > 0)) // Редактирование клиента
                {
                    MessageBox.Show("Выберите клиента для редактирования!");
                    return;
                }
                var selectedClientRow = dgvEmployees.SelectedRows[0];
                var employeeId = (int)selectedClientRow.Cells[0].Value;

                new AddEditEmployeeForm(employeeId).ShowDialog();
                RefreshClients();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnFire_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(dgvEmployees.SelectedRows.Count > 0)) // Редактирование клиента
                {
                    MessageBox.Show("Выберите клиента для редактирования!");
                    return;
                }
                var selectedClientRow = dgvEmployees.SelectedRows[0];
                var employeeId = (int)selectedClientRow.Cells[0].Value;

                using (var db = new MfiContext())
                {
                    var employee = db.Employee.First(em => em.Id == employeeId);
                    var account = employee.Account;

                    if (employee.Account.Login == LoginInfo.Login)
                    {
                        MessageBox.Show("Ишь чего удумали! А ну марш за работу!");
                        return;
                    }

                    db.Employee.Remove(employee);
                    db.Account.Remove(account);

                    db.SaveChanges();
                    MessageBox.Show("Сотрудник уволен!");
                }

                RefreshClients();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tbSmartSearch_TextChanged(object sender, EventArgs e)
        {
            RefreshClients();
        }

        private void cbMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshChart();
        }

        void RefreshClients()
        {
            using (var db = new MfiContext())
            {
                var query = db.Employee
                    .AsNoTracking()
                    .AsQueryable();

                var searchText = tbSmartSearch.Text;
                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    if (searchText.Any(e => char.IsDigit(e)))
                        query = query.Where(c => c.DateOfBirth.ToString().Contains(searchText));
                    else
                        query = query
                            .Where(e => e.Name.Contains(searchText) || e.Surname.Contains(searchText) || e.Account.Role.Name.Contains(searchText));
                }

                var employee = query
                    .Select(e => new
                    {
                        Id = e.Id,
                        Name = e.Name,
                        Surname = e.Surname,
                        DateOfBirth = e.DateOfBirth,
                        Sex = (e.Sex == "m" ? "Муж" : "Жен"),
                        Role = e.Account.Role.Name
                    })
                    .ToArray();

                dgvEmployees.DataSource = employee;
            }
        }

        void RefreshChart()
        {
            chLoans.Series[0].Points.Clear();

            var totalIncome = 0.0m;
            var totalLoss = 0.0m;
            int? currentMonth = cbMonth.SelectedIndex - 1; // Январь с индексом 1
            currentMonth = currentMonth < 0 ? null : currentMonth; // Проверяем, что выбран именно месяц а не пункт "Любой"
            var currentYear = DateTime.Now.Year;

            using (var db = new MfiContext())
            {
                var paymentsQuery = db.Payment
                    .AsNoTracking()
                    .AsQueryable();
                var loansQuery = db.Loan
                    .AsNoTracking()
                    .AsQueryable();

                paymentsQuery = paymentsQuery.Where(p => p.date.Year == currentYear);
                if (currentMonth.HasValue)
                    paymentsQuery = paymentsQuery.Where(p => p.date.Month == currentMonth.Value);
                loansQuery = loansQuery.Where(l => l.dateOfIssue.Year == currentYear);
                if (currentMonth.HasValue)
                    loansQuery = loansQuery.Where(l => l.dateOfIssue.Month == currentMonth.Value);

                totalIncome = paymentsQuery.ToArray().Sum(p => p.sum);
                totalLoss = loansQuery.ToArray().Sum(l => l.sum);
            }

            chLoans.Series[0].Points.AddY(totalIncome);
            chLoans.Series[0].Points[0].LegendText = "Доход";
            chLoans.Series[0].Points[0].IsValueShownAsLabel = true;
            //chLoans.Series[0].Label = "#PERCENT{p}";

            chLoans.Series[0].Points.AddY(totalLoss);
            chLoans.Series[0].Points[1].LegendText = "Расход";
            chLoans.Series[0].Points[1].IsValueShownAsLabel = true;
            //chLoans.Series[0].Label = "#PERCENT{p}";
        }

        void ShowLoginData()
        {
            lbName.Text = LoginInfo.Name;
            lbSurname.Text = LoginInfo.Surname;
            lbLogin.Text = LoginInfo.Login;
        }
    }
}

﻿using MfiDesktopApp.Context;
using MfiDesktopApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MfiDesktopApp.Forms
{
    public partial class AddEditEmployeeForm : Form
    {
        public int? EmployeeId { get; set; } = null;
        public bool AccountExists { get => EmployeeId.HasValue; }

        public AddEditEmployeeForm()
        {
            InitializeComponent();

            FillRoles();
            SetPasswordInputType();
        }

        public AddEditEmployeeForm(int id)
        {
            InitializeComponent();

            EmployeeId = id;
            FillRoles();
            HidePassword();
            ShowEmployeeData(id);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                using (var db = new MfiContext())
                {
                    Employee employee = null;
                    Account account = null;
                    if (!EmployeeId.HasValue)
                    {
                        employee = new Employee();
                        account = new Account();
                    }
                    else
                    {
                        employee = db.Employee.First(em => em.Id == EmployeeId.Value);
                        account = employee.Account;
                    }

                    employee.Name = tbName.Text;
                    employee.Surname = tbSurname.Text;
                    employee.Sex = cbSex.SelectedIndex == 0 ? "m" : "f";
                    employee.DateOfBirth = dtpDateOfBirth.Value;

                    account.Login = tbLogin.Text;
                    account.PhoneNumber = mtbPhone.Text;
                    account.Mail = tbMail.Text;
                    account.Role = db.Role.First(r => r.Name == cbRole.SelectedItem);
                    if (!AccountExists)
                        account.Password = tbPassword.Text;

                    if (!AccountExists)
                        db.Account.Add(account);

                    if (!EmployeeId.HasValue)
                    {
                        employee.Account = account;
                        db.Employee.Add(employee);
                    }

                    db.SaveChanges();

                    if (!EmployeeId.HasValue)
                        MessageBox.Show("Новый сотрудник успешно добавлен!");
                    else
                        MessageBox.Show("Сотрудник успешно отредактирован!");

                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void cbHidePassword_CheckedChanged(object sender, EventArgs e)
        {
            SetPasswordInputType();
        }

        void SetPasswordInputType()
        {
            if (cbHidePassword.Checked == true)
            {
                tbPassword.UseSystemPasswordChar = PasswordPropertyTextAttribute.Yes.Password;
                tbPassword.PasswordChar = '●';
            }
            else
            {
                tbPassword.UseSystemPasswordChar = PasswordPropertyTextAttribute.No.Password;
                tbPassword.PasswordChar = '\0';
            }
        }

        void ShowEmployeeData(int id)
        {
            using (var db = new MfiContext())
            {
                var employee = db.Employee.First((System.Linq.Expressions.Expression<Func<Employee, bool>>)(e => e.Id == id));
                var account = employee.Account;

                tbName.Text = employee.Name;
                tbSurname.Text = employee.Surname;
                cbSex.SelectedIndex = employee.Sex == "m" ? 0 : 1;
                dtpDateOfBirth.Value = employee.DateOfBirth;

                tbLogin.Text = account.Login;
                tbPassword.Text = account.Password;
                tbMail.Text = account.Mail;
                mtbPhone.Text = account.PhoneNumber;
                cbRole.SelectedItem = account.Role.Name;
            }
        }

        void FillRoles()
        {
            using (var db = new MfiContext())
            {
                var roles = db.Role.Select(r => r.Name).ToArray();
                cbRole.Items.Clear();
                cbRole.Items.AddRange(roles);
                cbRole.SelectedIndex = 0;
            }
        }

        void HidePassword()
        {
            tbPassword.Enabled = false;
            tbPassword.Visible = false;

            lbPassword.Enabled = false;
            lbPassword.Visible = false;

            cbHidePassword.Enabled = false;
            cbHidePassword.Visible = false;
        }
    }
}

﻿
namespace MfiDesktopApp.Forms
{
    partial class TakeLoanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbSum = new System.Windows.Forms.TextBox();
            this.dtpDateOfIssue = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nudMonthsToRepay = new System.Windows.Forms.NumericUpDown();
            this.nudPercent = new System.Windows.Forms.NumericUpDown();
            this.btnTakeLoan = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudMonthsToRepay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPercent)).BeginInit();
            this.SuspendLayout();
            // 
            // tbSum
            // 
            this.tbSum.Location = new System.Drawing.Point(177, 77);
            this.tbSum.Name = "tbSum";
            this.tbSum.Size = new System.Drawing.Size(162, 20);
            this.tbSum.TabIndex = 0;
            // 
            // dtpDateOfIssue
            // 
            this.dtpDateOfIssue.Location = new System.Drawing.Point(177, 139);
            this.dtpDateOfIssue.Name = "dtpDateOfIssue";
            this.dtpDateOfIssue.Size = new System.Drawing.Size(162, 20);
            this.dtpDateOfIssue.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Сумма";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Процент";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Начало действия";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 177);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Месяцев на погашение";
            // 
            // nudMonthsToRepay
            // 
            this.nudMonthsToRepay.Location = new System.Drawing.Point(177, 170);
            this.nudMonthsToRepay.Name = "nudMonthsToRepay";
            this.nudMonthsToRepay.Size = new System.Drawing.Size(162, 20);
            this.nudMonthsToRepay.TabIndex = 3;
            // 
            // nudPercent
            // 
            this.nudPercent.Location = new System.Drawing.Point(177, 107);
            this.nudPercent.Name = "nudPercent";
            this.nudPercent.Size = new System.Drawing.Size(162, 20);
            this.nudPercent.TabIndex = 3;
            // 
            // btnTakeLoan
            // 
            this.btnTakeLoan.Location = new System.Drawing.Point(263, 257);
            this.btnTakeLoan.Name = "btnTakeLoan";
            this.btnTakeLoan.Size = new System.Drawing.Size(75, 23);
            this.btnTakeLoan.TabIndex = 4;
            this.btnTakeLoan.Text = "Взять";
            this.btnTakeLoan.UseVisualStyleBackColor = true;
            this.btnTakeLoan.Click += new System.EventHandler(this.btnTakeLoan_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(182, 257);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // TakeLoanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 319);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnTakeLoan);
            this.Controls.Add(this.nudPercent);
            this.Controls.Add(this.nudMonthsToRepay);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpDateOfIssue);
            this.Controls.Add(this.tbSum);
            this.Name = "TakeLoanForm";
            this.Text = "Взять кредит";
            ((System.ComponentModel.ISupportInitialize)(this.nudMonthsToRepay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPercent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbSum;
        private System.Windows.Forms.DateTimePicker dtpDateOfIssue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudMonthsToRepay;
        private System.Windows.Forms.NumericUpDown nudPercent;
        private System.Windows.Forms.Button btnTakeLoan;
        private System.Windows.Forms.Button btnCancel;
    }
}
﻿using MfiDesktopApp.Context;
using MfiDesktopApp.Enums;
using MfiDesktopApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MfiDesktopApp.Forms
{
    public partial class TakeLoanForm : Form
    {
        public int ClientId { get; set; }

        public TakeLoanForm(int clientId)
        {
            InitializeComponent();
            ClientId = clientId;
            // TODO: Настроить грёбаные табы!
            // TODO: Сделать историю
            // TODO: Валидация
            // TODO: Красота
            // TODO: Диалоговое окно "точно ли хотите взять кредит?"
            // TODO: Данные о клиенте
            // TODO: Предварительные данные о кредите (график платежей там или что-то такое)
        }

        private void btnTakeLoan_Click(object sender, EventArgs e)
        {
            try
            {
                using (var db = new MfiContext())
                {
                    var loan = new Loan()
                    {
                        sum = decimal.Parse(tbSum.Text),
                        percent = nudPercent.Value,
                        dateOfIssue = dtpDateOfIssue.Value,
                        timeLimitationInMonths = null,
                    };
                    if (nudMonthsToRepay.Value != 0)
                        loan.timeLimitationInMonths = (int)nudMonthsToRepay.Value;

                    loan.ClientId = ClientId;
                    loan.StatusId = (int)LoanStatusEnum.Active;

                    db.Loan.Add(loan);
                    db.SaveChanges();
                    MessageBox.Show("Займ успешно оформлен!"); // TODO: Добавить указание на кого он оформлен
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}

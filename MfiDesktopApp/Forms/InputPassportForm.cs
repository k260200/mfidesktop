﻿using MfiDesktopApp.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MfiDesktopApp.Forms
{
    public partial class InputPassportForm : Form
    {
        public string Passport { get; set; }

        public InputPassportForm()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            using (var db = new MfiContext())
            {
                var passport = mtbPassport.Text;
                if (!db.Client.Any(c => c.PassportSerialNumber == passport))
                {
                    MessageBox.Show("Такого паспорта нет в нашей базе.");
                    return;
                }
            }

            Passport = mtbPassport.Text;

            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}

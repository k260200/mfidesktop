﻿using MfiDesktopApp.Context;
using MfiDesktopApp.Dto;
using MfiDesktopApp.Enums;
using MfiDesktopApp.Models;
using MfiDesktopApp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MfiDesktopApp.Forms
{
    public partial class SignInForm : Form
    {
        public LoginDto LoginData { get; set; }
        public bool ClientExists { get; set; } = false;

        public SignInForm(string passport)
        {
            InitializeComponent();
            FillSex();

            ShowClientInfo(passport);
            LockClientInfoInput();
            ClientExists = true;

            SetPasswordInputType();
        }

        public SignInForm()
        {
            // TODO: Настроить грёбаные табы!
            // TODO: Согласие на обработку персональных данных
            InitializeComponent();
            FillSex();

            SetPasswordInputType();
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {
            try
            {
                var account = new Account();
                var client = new Client();
                account.RoleId = (int)RoleEnum.Client;

                account.Login = tbLogin.Text;
                account.Password = tbPassword.Text; // TODO: Вот тут проще всего будет хэшировать наверное?
                account.PhoneNumber = mtbPhone.Text;
                account.Mail = tbMail.Text;

                client.Name = tbName.Text;
                client.Surname = tbSurname.Text;
                client.Sex = cbSex.SelectedIndex == 0 ? "m" : "f";
                client.DateOfBirth = dtpDateOfBirth.Value;
                client.CurrentIncome = decimal.Parse(tbIncome.Text);
                client.PassportSerialNumber = mtbPassportSerialNumber.Text;
                client.PassportDateOfIssue = dtpPassportDateOfIssue.Value;

                LoginData = SignInService.SignIn(account, client);

                if (LoginData != null)
                    this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void cbHidePassword_CheckedChanged(object sender, EventArgs e)
        {
            SetPasswordInputType();
        }

        void SetPasswordInputType()
        {
            if (cbHidePassword.Checked == true)
            {
                tbPassword.UseSystemPasswordChar = PasswordPropertyTextAttribute.Yes.Password;
                tbPassword.PasswordChar = '●';
            }
            else
            {
                tbPassword.UseSystemPasswordChar = PasswordPropertyTextAttribute.No.Password;
                tbPassword.PasswordChar = '\0';
            }
        }

        void FillSex()
        {
            cbSex.Items.Clear();
            cbSex.Items.AddRange(new[] { "Муж", "Жен" });
            cbSex.SelectedIndex = 0;
        }

        void ShowClientInfo(string passport)
        {
            using (var db = new MfiContext())
            {
                var client = db.Client.First(c => c.PassportSerialNumber == passport);

                tbName.Text = client.Name;
                tbSurname.Text = client.Surname;
                cbSex.SelectedIndex = client.Sex == "m" ? 0 : 1;
                dtpDateOfBirth.Value = client.DateOfBirth;
                tbIncome.Text = client.CurrentIncome.ToString();
                mtbPassportSerialNumber.Text = client.PassportSerialNumber;
                dtpPassportDateOfIssue.Value = client.PassportDateOfIssue;
            }
        }

        void LockClientInfoInput()
        {
            tbName.Enabled = false;
            tbSurname.Enabled = false;
            cbSex.Enabled = false;
            dtpDateOfBirth.Enabled = false;
            tbIncome.Enabled = false;
            mtbPassportSerialNumber.Enabled = false;
            dtpPassportDateOfIssue.Enabled = false;
        }
    }
}

﻿using MfiDesktopApp.Context;
using MfiDesktopApp.Enums;
using MfiDesktopApp.Models;
using MfiDesktopApp.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MfiDesktopApp.Forms
{
    public partial class AddPaymentForm : Form
    {
        // TODO: Настроить грёбаные табы!
        // TODO: Сделать историю
        // TODO: Валидация
        // TODO: Красота
        // TODO: Диалоговое окно "точно ли хотите оплатить?"
        // TODO: Данные о клиенте
        public LoanInfo LoanToPayFor { get; set; }
        public int ClientId { get; set; }
        public int LoanId { get; set; }

        public AddPaymentForm(int clientId, int loanId)
        {
            InitializeComponent();

            var loanInfo = new LoanInfo();
            using (var db = new MfiContext())
            {
                var selectedLoan = db.Loan
                .First(l => l.Id == loanId);

                loanInfo.Id = selectedLoan.Id;
                loanInfo.Sum = selectedLoan.sum;
                loanInfo.Percent = selectedLoan.percent;
                loanInfo.CurrentSum = selectedLoan.CurrentSum;
                loanInfo.Repaid = selectedLoan.RedemptionSum;
                loanInfo.Taken = selectedLoan.dateOfIssue;
                loanInfo.MustBeRepaid = selectedLoan.dateOfRedemotion;
                loanInfo.Status = selectedLoan.LoanStatusName;
                loanInfo.Passport = db.Client.First(c => c.Id == clientId).PassportSerialNumber;
            }

            LoanToPayFor = loanInfo;
            ClientId = clientId;
            LoanId = loanId;

            pgLoanInfo.SelectedObject = LoanToPayFor;
        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            try
            {
                using (var db = new MfiContext())
                {
                    var payment = new Payment()
                    {
                        sum = decimal.Parse(tbSum.Text),
                        ClientId = ClientId,
                        LoanId = LoanId,
                        StatusId = (int)PaymentStatusEnum.Active
                    };

                    db.Payment.Add(payment);
                    db.SaveChanges();

                    MessageBox.Show("Платёж успешно проведён!");
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}

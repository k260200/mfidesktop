﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MfiDesktopApp.Forms
{
    public partial class ScreenSplash : Form
    {
        public ScreenSplash()
        {
            InitializeComponent();
        }

        private void ScreenSplash_Shown(object sender, EventArgs e)
        {
            this.Refresh();
            Thread.Sleep(1000);
            this.DialogResult = DialogResult.OK;
        }
    }
}

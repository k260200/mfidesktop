﻿using MfiDesktopApp.Context;
using MfiDesktopApp.Dto;
using MfiDesktopApp.Enums;
using MfiDesktopApp.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MfiDesktopApp.Forms
{
    public partial class CashierForm : Form
    {
        // TODO: Показывать кто авторизован
        public LoginDto LoginInfo { get; set; }
        public string SelectedPassport { get; set; }
        public PaymentStatusEnum? Status { get; set; }
        public int? LoanId { get; set; }

        public CashierForm(LoginDto loginInfo)
        {
            InitializeComponent();

            LoginInfo = loginInfo;
            ShowLoginData();

            FillPassports();
            FillLoans();
            FillStatuses();
            RefreshPayments();
        }

        private void btnAddPayment_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(SelectedPassport) || !LoanId.HasValue)
                {
                    MessageBox.Show("Выберите паспорт и займ чтобы внести платёж!");
                    return;
                }

                int clientId = 0;
                using (var db = new MfiContext())
                {
                    var client = db.Client.FirstOrDefault(c => c.PassportSerialNumber == SelectedPassport);
                    if (client == null)
                        throw new Exception("Введите или выберите корректный паспорт!");

                    clientId = client.Id;
                }

                new AddPaymentForm(clientId, LoanId.Value).ShowDialog();
                RefreshPayments();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancelPayment_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(dgvPayments.SelectedRows.Count > 0))
                {
                    MessageBox.Show("Выберите платёж чтобы отозвать!");
                    return;
                }

                var selectedPaymentRow = dgvPayments.SelectedRows[0];
                var paymentId = (int)selectedPaymentRow.Cells[0].Value;

                using (var db = new MfiContext())
                {
                    var selectedPayment = db.Payment
                        .First(p => p.Id == paymentId);

                    if (selectedPayment.StatusId == (int)PaymentStatusEnum.Cancelled)
                    {
                        MessageBox.Show("Платёж уже был отменён!");
                        return;
                    }

                    selectedPayment.StatusId = (int)PaymentStatusEnum.Cancelled;
                    db.SaveChanges();
                    MessageBox.Show("Платёж успешно отменён!");
                    RefreshPayments();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cbPassport_SelectedValueChanged(object sender, EventArgs e)
        {
            SelectedPassport = cbPassport.Text;
            FillLoans();
            RefreshPayments();
        }

        private void cbLoan_SelectedValueChanged(object sender, EventArgs e)
        {
            var loan = cbLoan.SelectedItem;
            if (!(loan is LoanLine))
                LoanId = null;
            else
                LoanId = (cbLoan.SelectedItem as LoanLine).Id;
            RefreshPayments();
        }

        private void cbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            var selectedString = (string)cbStatus.SelectedItem;

            switch (selectedString)
            {
                case "Активен": Status = PaymentStatusEnum.Active; break;
                case "Отменён": Status = PaymentStatusEnum.Cancelled; break;
                case "Любой": Status = null; break;
                default: Status = null; break;
            }
            RefreshPayments();
        }

        void RefreshPayments()
        {
            using (var db = new MfiContext())
            {
                var query = db.Payment
                    .AsNoTracking()
                    .AsQueryable();

                if (!string.IsNullOrWhiteSpace(SelectedPassport))
                    query = query.Where(p => p.Client.PassportSerialNumber.Contains(SelectedPassport));

                if (LoanId.HasValue)
                    query = query.Where(p => p.LoanId == LoanId.Value);

                if (Status.HasValue)
                    query = query.Where(p => p.StatusId == (int)Status);

                var payments = query
                    .Select(p => new
                    {
                        Id = p.Id,
                        sum = p.sum,
                        date = p.date,
                        LoanId = p.LoanId,
                        Client = p.Client.PassportSerialNumber,
                        Status = p.PaymentStatus.Name
                    })
                    .ToArray();
                dgvPayments.DataSource = payments;
            }
        }

        void FillPassports()
        {
            string[] passports = null;
            using (var db = new MfiContext())
            {
                passports = db.Client
                    .Select(c => c.PassportSerialNumber)
                    .Distinct()
                    .ToArray();
            }

            cbPassport.Items.Clear();
            cbPassport.Items.AddRange(passports);
            cbPassport.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        void FillLoans()
        {
            LoanLine[] loans = null;
            using (var db = new MfiContext())
            {
                var query = db.Loan.AsQueryable();

                if (!string.IsNullOrWhiteSpace(SelectedPassport))
                    query = query.Where(l => l.Client.PassportSerialNumber.Contains(SelectedPassport));

                loans = query.Select(l => new LoanLine
                {
                    Id = l.Id,
                    Sum = l.sum,
                    Percent = l.percent,
                    Taken = l.dateOfIssue
                }).ToArray();
            }

            cbLoan.Items.Clear();
            cbLoan.Items.Add("Любой");
            cbLoan.Items.AddRange(loans);
            cbLoan.SelectedIndex = 0;
            cbLoan.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        void FillStatuses()
        {
            cbStatus.Items.Clear();
            cbStatus.Items.AddRange(new[] { "Активен", "Отменён", "Любой" });
            cbStatus.SelectedIndex = 0;
            cbStatus.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        void ShowLoginData()
        {
            lbName.Text = LoginInfo.Name;
            lbSurname.Text = LoginInfo.Surname;
            lbLogin.Text = LoginInfo.Login;
        }
    }
}

﻿using MfiDesktopApp.Context;
using MfiDesktopApp.Enums;
using MfiDesktopApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MfiDesktopApp.Forms
{
    public partial class AddEditClientForm : Form
    {
        public int? ClientId { get; set; }

        public AddEditClientForm()
        {
            InitializeComponent();
            FillSex();

            ClientId = null;
        }

        public AddEditClientForm(int clientId)
        {
            InitializeComponent();
            FillSex();

            ClientId = clientId;
            ShowClientInfo();
        }

        private void btnOk_Click(object sender, EventArgs e) // TODO: Валидация (стырить из регистрации)
        {
            try
            {
                using (var db = new MfiContext())
                {
                    Client employee = null;
                    if (!ClientId.HasValue)
                        employee = new Client();
                    else
                    {
                        employee = db.Client.First(c => c.Id == ClientId.Value);
                    }

                    employee.Name = tbName.Text;
                    employee.Surname = tbSurname.Text;
                    employee.Sex = cbSex.SelectedIndex == 0 ? "m" : "f";
                    employee.DateOfBirth = dtpDateOfBirth.Value;
                    employee.CurrentIncome = decimal.Parse(tbIncome.Text);
                    employee.PassportSerialNumber = mtbPassportSerialNumber.Text;
                    employee.PassportDateOfIssue = dtpPassportDateOfIssue.Value;

                    if (!ClientId.HasValue)
                        db.Client.Add(employee);

                    db.SaveChanges();

                    if (!ClientId.HasValue)
                        MessageBox.Show("Новый клиент успешно добавлен!");
                    else
                        MessageBox.Show("Клиент успешно отредактирован!");

                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        void ShowClientInfo()
        {
            if (!ClientId.HasValue)
                return;

            using (var db = new MfiContext())
            {
                var client = db.Client.First(c => c.Id == ClientId.Value);

                tbName.Text = client.Name;
                tbSurname.Text = client.Surname;
                cbSex.SelectedIndex = client.Sex == "m" ? 0 : 1;
                dtpDateOfBirth.Value = client.DateOfBirth;
                tbIncome.Text = client.CurrentIncome.ToString();
                mtbPassportSerialNumber.Text = client.PassportSerialNumber;
                dtpPassportDateOfIssue.Value = client.PassportDateOfIssue;
            }
        }

        void FillSex()
        {
            cbSex.Items.Clear();
            cbSex.Items.AddRange(new[] { "Муж", "Жен"});
            cbSex.SelectedIndex = 0;
        }
    }
}

﻿using MfiDesktopApp.Context;
using MfiDesktopApp.Dto;
using MfiDesktopApp.Enums;
using MfiDesktopApp.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MfiDesktopApp.Forms
{
    public partial class ManagerForm : Form
    {
        // TODO: Допилить продажу долга коллекторам, если время останется
        public LoginDto LoginInfo { get; set; }
        public string SelectedPassport { get; set; }
        public LoanStatusEnum? Status { get; set; }

        public ManagerForm(LoginDto loginInfo)
        {
            InitializeComponent();

            LoginInfo = loginInfo;
            ShowLoginData();

            RefreshData();
        }

        private void btnAddClient_Click(object sender, EventArgs e)
        {
            try
            {
                new AddEditClientForm().ShowDialog();
                RefreshData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEditClient_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(dgvClients.SelectedRows.Count > 0)) // Редактирование клиента
                {
                    MessageBox.Show("Выберите клиента для редактирования!");
                    return;
                }
                var selectedClientRow = dgvClients.SelectedRows[0];
                var clientId = (int)selectedClientRow.Cells[0].Value;

                new AddEditClientForm(clientId).ShowDialog();
                RefreshData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnTakeLoan_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(dgvClients.SelectedRows.Count > 0))
                {
                    MessageBox.Show("Выберите клиента для оформления кредита!");
                    return;
                }
                var selectedClientRow = dgvClients.SelectedRows[0];
                var clientId = (int)selectedClientRow.Cells[0].Value;

                new TakeLoanForm(clientId).ShowDialog();

                RefreshLoans();
                RefreshLoanInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tbSmartSearch_TextChanged(object sender, EventArgs e)
        {
            RefreshClients();
        }

        private void dgvClients_SelectionChanged(object sender, EventArgs e)
        {
            RefreshClientInfo();
        }

        private void cbPassport_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedPassport = cbPassport.Text;
            RefreshLoans();
            RefreshLoanInfo();
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedString = (string)cbStatus.SelectedItem;

            switch (selectedString)
            {
                case "Активен": Status = LoanStatusEnum.Active; break;
                case "Отменён": Status = LoanStatusEnum.Cancelled; break;
                case "Выплачен": Status = LoanStatusEnum.Repaid; break;
                //case "Отменён": Status = LoanStatusEnum.SoldToCollectors; break; TODO: Добавить, если буду делать продажу долга
                case "Любой": Status = null; break;
                default: Status = null; break;
            }
            RefreshLoans();
            RefreshLoanInfo();
        }

        private void btnCancelLoan_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(dgvLoans.SelectedRows.Count > 0))
                {
                    MessageBox.Show("Выберите платёж чтобы отозвать!");
                    return;
                }

                var selectedLoanRow = dgvLoans.SelectedRows[0];
                var loanId = (int)selectedLoanRow.Cells[0].Value;

                using (var db = new MfiContext())
                {
                    var selectedLoan = db.Loan
                        .First(p => p.Id == loanId);

                    if (selectedLoan.StatusId == (int)LoanStatusEnum.Cancelled)
                    {
                        MessageBox.Show("Кредит уже был отменён!");
                        return;
                    }

                    selectedLoan.StatusId = (int)PaymentStatusEnum.Cancelled;
                    db.SaveChanges();
                    MessageBox.Show("Кредит успешно отменён!");
                    RefreshData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvLoans_SelectionChanged(object sender, EventArgs e)
        {
            RefreshLoanInfo();
        }

        void RefreshData()
        {
            FillPassports();
            FillStatuses();

            RefreshClients();
            RefreshLoans();
            RefreshClientInfo();
            RefreshLoanInfo();
        }

        void RefreshClients()
        {
            using (var db = new MfiContext())
            {
                var query = db.Client
                    .AsNoTracking()
                    .AsQueryable();

                var searchText = tbSmartSearch.Text;
                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    if (searchText.Any(c => char.IsDigit(c)))
                        query = query.Where(c => c.PassportSerialNumber.Contains(searchText));
                    else
                        query = query
                            .Where(c => c.Name.Contains(searchText) || c.Surname.Contains(searchText));
                }

                var clients = query
                    .Select(c => new
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Surname = c.Surname,
                        PassportSerialNumber = c.PassportSerialNumber
                    })
                    .ToArray();

                dgvClients.DataSource = clients;
            }
        }

        void RefreshClientInfo() // TODO: Разобраться как запретить редактирование не отключая контрол полностью
        {
            if (dgvClients.SelectedRows.Count > 0)
            {
                var selectedClientRow = dgvClients.SelectedRows[0];
                var clientId = (int)selectedClientRow.Cells[0].Value;

                using (var db = new MfiContext())
                {
                    var selectedClient = db.Client
                        .First(c => c.Id == clientId);
                    var clientInfo = new ClientInfo(selectedClient);

                    pgClientInfo.SelectedObject = clientInfo;
                }
            }
        }

        void RefreshLoans()
        {
            using (var db = new MfiContext())
            {
                var query = db.Loan
                    .AsNoTracking()
                    .AsQueryable();

                if (!string.IsNullOrWhiteSpace(SelectedPassport))
                    query = query.Where(p => p.Client.PassportSerialNumber.Contains(SelectedPassport));

                if (Status.HasValue)
                    query = query.Where(p => p.StatusId == (int)Status);

                var loans = query
                    .Select(l => new
                    {
                        Id = l.Id,
                        sum = l.sum,
                        percent = l.percent,
                        Client = l.Client.PassportSerialNumber,
                        Status = l.LoanStatus.Name
                    })
                    .ToArray();
                dgvLoans.DataSource = loans;
            }
        }

        void RefreshLoanInfo()
        {
            if (dgvLoans.SelectedRows.Count > 0)
            {
                var selectedLoanRow = dgvLoans.SelectedRows[0];
                var loanId = (int)selectedLoanRow.Cells[0].Value;
                var passport = selectedLoanRow.Cells[2].Value.ToString();

                using (var db = new MfiContext())
                {
                    var selectedLoan = db.Loan
                    .First(l => l.Id == loanId);
                    var loanInfo = new LoanInfo
                    {
                        Id = selectedLoan.Id,
                        Sum = selectedLoan.sum,
                        Percent = selectedLoan.percent,
                        CurrentSum = selectedLoan.CurrentSum,
                        Repaid = selectedLoan.RedemptionSum,
                        Taken = selectedLoan.dateOfIssue,
                        MustBeRepaid = selectedLoan.dateOfRedemotion,
                        Status = selectedLoan.LoanStatusName,
                        Passport = passport
                    };
                    pgLoanInfo.SelectedObject = loanInfo;
                }
            }
        }

        void FillPassports() // TODO: Починить, сделав поиск в real-time или дав возможность выбрать любой паспорт
        {
            string[] passports = null;
            using (var db = new MfiContext())
            {
                passports = db.Client
                    .Select(c => c.PassportSerialNumber)
                    .Distinct()
                    .ToArray();
            }

            cbPassport.Items.Clear();
            cbPassport.Items.AddRange(passports);
            cbPassport.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        void FillStatuses()
        {
            cbStatus.Items.Clear();
            cbStatus.Items.AddRange(new[] { "Активен", "Отменён", "Выплачен" ,"Любой" });
            cbStatus.SelectedIndex = 0;
            cbStatus.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        void ShowLoginData()
        {
            lbName.Text = LoginInfo.Name;
            lbSurname.Text = LoginInfo.Surname;
            lbLogin.Text = LoginInfo.Login;
        }
    }
}

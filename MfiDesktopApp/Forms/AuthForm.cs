﻿using MfiDesktopApp.Dto;
using MfiDesktopApp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MfiDesktopApp.Forms
{
    public partial class AuthForm : Form
    {
        public LoginDto LoginData { get; set; }

        public AuthForm()
        {
            InitializeComponent();
            SetPasswordInputType();
        }

        private void cbHidePassword_CheckedChanged(object sender, EventArgs e)
        {
            SetPasswordInputType();
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            try
            {
                LoginData = LoginService.TryLogin(tbLogin.Text, tbPassword.Text);

                if (LoginData != null)
                    this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialog = MessageBox.Show("Вы уже брали у нас кредиты?", "Регистрация", MessageBoxButtons.YesNo);
                if (dialog == DialogResult.Yes)
                {
                    var inputForm = new InputPassportForm();
                    inputForm.ShowDialog();

                    if (inputForm.DialogResult == DialogResult.OK)
                    {
                        var passport = inputForm.Passport;
                        var signInForm = new SignInForm(passport);
                        signInForm.ShowDialog();
                        LoginData = signInForm.LoginData;

                        if (LoginData != null)
                            this.DialogResult = DialogResult.OK;
                    }
                }
                else if (dialog == DialogResult.No)
                {
                    var signInForm = new SignInForm();
                    signInForm.ShowDialog();
                    LoginData = signInForm.LoginData;

                    if (LoginData != null)
                        this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void SetPasswordInputType()
        {
            if (cbHidePassword.Checked == true)
            {
                tbPassword.UseSystemPasswordChar = PasswordPropertyTextAttribute.Yes.Password;
                tbPassword.PasswordChar = '●';
            }
            else
            {
                tbPassword.UseSystemPasswordChar = PasswordPropertyTextAttribute.No.Password;
                tbPassword.PasswordChar = '\0';
            }
        }
    }
}

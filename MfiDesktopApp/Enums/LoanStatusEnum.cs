﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MfiDesktopApp.Enums
{
    public enum LoanStatusEnum
    {
        Active = 1,
        Cancelled = 2,
        Repaid = 3,
        SoldToCollectors = 4
    }
}

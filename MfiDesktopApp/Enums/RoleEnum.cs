﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MfiDesktopApp.Enums
{
    public enum RoleEnum
    {
        Client = 1,
        Cashier = 2,
        LoanManager = 3,
        Director = 4
    }
}

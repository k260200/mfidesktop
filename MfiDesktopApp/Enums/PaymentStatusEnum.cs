﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MfiDesktopApp.Enums
{
    public enum PaymentStatusEnum
    {
        Active = 1,
        Cancelled = 2
    }
}

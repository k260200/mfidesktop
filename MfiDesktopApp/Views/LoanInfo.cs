﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MfiDesktopApp.Views
{
    // TODO: Добавить аттрибутов чтобы было красиво
    //[DisplayName(...)]
    //[Description(...)]
    //[Category(...)]
    //[TypeConverter(...)]
    //[ReadOnly(...)]
    //[Browsable(...)]
    //[DefaultValue(...)]
    //[Editor(...)]

    public class LoanInfo
    {
        // TODO: Сделать нормальный конструктор
        public LoanInfo() { }

        [DisplayName("Уникальный №")]
        public int Id { get; set; }

        [DisplayName("Сумма")]
        public decimal Sum { get; set; }

        [DisplayName("Процент")]
        public decimal Percent { get; set; }

        [DisplayName("Текущая сумма")]
        public decimal CurrentSum { get; set; }

        [DisplayName("Выплачено")]
        public decimal Repaid { get; set; }

        [DisplayName("Взят")]
        public DateTime Taken { get; set; }

        [DisplayName("Должен быть возвращён")]
        public DateTime? MustBeRepaid { get; set; }

        [DisplayName("Статус")]
        public string Status { get; set; }

        [DisplayName("Паспорт заёмщика")]
        public string Passport { get; set; }
    }
}

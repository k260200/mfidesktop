﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MfiDesktopApp.Views
{
    public class LoanLine
    {
        public LoanLine() { }

        [DisplayName("Уникальный №")]
        public int Id { get; set; }

        [DisplayName("Сумма")]
        public decimal Sum { get; set; }

        [DisplayName("Процент")]
        public decimal Percent { get; set; }

        [DisplayName("Взят")]
        public DateTime Taken { get; set; }

        [DisplayName("Должен быть возвращён")]
        public DateTime? MustBeRepaid { get; set; }

        public override string ToString()
        {
            return $"№{Id} на {Sum} под {Percent} от {Taken.Date}";
        }
    }
}

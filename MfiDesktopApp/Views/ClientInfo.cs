﻿using MfiDesktopApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MfiDesktopApp.Views
{
    public class ClientInfo
    {
        public ClientInfo(){}

        public ClientInfo(Client client)
        {
            Id = client.Id;
            Name = client.Name;
            Surname = client.Surname;
            DateOfBirth = client.DateOfBirth;
            Sex = client.Sex == "f" ? "Жен" : "Муж";
            PassportSerialNumber = client.PassportSerialNumber;
            PassportDateOfIssue = client.PassportDateOfIssue;
            Income = client.CurrentIncome;
            HasAccount = client.HasAccount ? "Да" : "Нет";
            ActiveLoansCount = client.ActiveLoansCount;
            CurrentDebt = client.CurrentDebt;
        }

        [DisplayName("Уникальный №")]
        public int Id { get; set; }

        [DisplayName("Имя")]
        public string Name { get; set; }

        [DisplayName("Фамилия")]
        public string Surname { get; set; }

        [DisplayName("Дата рождения")]
        public DateTime DateOfBirth { get; set; }

        [DisplayName("Пол")]
        public string Sex { get; set; }

        [Category("Паспорт")]
        [DisplayName("Серия и номер")]
        public string PassportSerialNumber { get; set; }

        [Category("Паспорт")]
        [DisplayName("Выдан")]
        public DateTime PassportDateOfIssue { get; set; }

        [Category("Благонадёжность")]
        [DisplayName("Доход")]
        public decimal Income { get; set; }

        [Category("Благонадёжность")]
        [DisplayName("Имеет учётную запись")]
        public string HasAccount { get; set; }

        [Category("Благонадёжность")]
        [DisplayName("Активных займов")]
        public int ActiveLoansCount { get; set; }

        [Category("Благонадёжность")]
        [DisplayName("Совокупный долг")]
        public decimal CurrentDebt { get; set; }
    }
}
